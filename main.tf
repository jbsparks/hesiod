resource "docker_container" "ccpe_sshd" {
  image = "cray/ccpe-devel-sles15sp2-x86_64:22.02-021622"
  name  = "ccpe_sshd"
  restart = "always"
  volumes {
    container_path  = "/home/guest"
    # replace the host_path with full path for your project directory starting from root directory /
    host_path = "/Users/sparksjo/workarea/EaaS" 
    read_only = false
  }
  ports {
    internal = 22
    external = 2222
  }
}
